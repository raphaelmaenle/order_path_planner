#include "order_path_planner/order_path_planner.hpp"


using std::placeholders::_1;

using namespace std::chrono_literals;

OrderPathPlanner::OrderPathPlanner(int argc, char** argv): Node("order_path_planner") {
    std::cout << "initializing... " << std::endl;
    auto path = arg_parser(argc, argv);

    data_handler = std::make_shared<DataHandler>(path);
    business_logic = std::make_shared<BusinessLogic>(data_handler);

    this->create_listeners();
    this->create_publishers();

    std::cout << "done" << std::endl;
}

OrderPathPlanner::OrderPathPlanner(): OrderPathPlanner(1, nullptr) {
}

std::string OrderPathPlanner::get_default_path() {
    return FilePath("test/data/").absolute_path();
}

std::string OrderPathPlanner::arg_parser(int argc, char** argv) {
    if(argc == 1) {
        print_usage();
        return get_default_path();

    } else if(argc > 2) {
        std::cout << "to many arguments!" << std::endl;
        print_usage();
        return get_default_path();

    } else if(!FilePath(argv[1]).valid_source_path()) {
        std::cout << "not a valid resource path" << std::endl;
        print_usage();
        return get_default_path();

    } else {
        std::cout << "using folder" << std::endl;
        return argv[1];
    }
}


void OrderPathPlanner::print_usage() {
    std::cout << "Usage: order_path_planner <configs root folder>" << std::endl;
    std::cout << "Using Default Test Folder \'test/data\' instead" << std::endl;
}


DataHandler::DataHandler(std::string path) {
    map = std::make_shared<Map>();
    conf_path =  get_conf_path(path);
    order_path = get_orders_path(path);
    product_collector = std::make_unique<ProductCollector>(conf_path);
    product_collector->collect_products_from_folder();
}

std::string DataHandler::get_conf_path(std::string path) {
    return FilePath(path, "/configuration/").get_path();
}

std::string DataHandler::get_orders_path(std::string path) {
    return FilePath(path, "/orders/").get_path();
}


std::pair<bool, std::shared_ptr<Order>> DataHandler::get_order_from_id(std::string order_id) {
    AsyncOrderFinder finder(order_path);
    auto success_order = finder.find_order_in_folder(order_id);
    order_id = "hello";
    return success_order;
}

std::vector<std::shared_ptr<Part>> DataHandler::get_parts_from_order(std::shared_ptr<Order> order) {
    std::vector<std::shared_ptr<Part>> parts;
    for(auto& product: this->product_collector->get_products()) {
        for(auto& required: order->product_names) {
            if(product.id == required) {
                for(auto& part: product.parts) {
                    parts.push_back(part);
                }
            }
        }
    }
    return parts;
}

void OrderPathPlanner::create_listeners() {
    this->create_position_listener();
    this->create_next_order_listener();
}

void OrderPathPlanner::create_publishers() {
    this->create_marker_publisher();
    this->create_tf_publisher();
}

void OrderPathPlanner::create_tf_publisher() {
    tf_publisher_ = std::make_shared<tf2_ros::TransformBroadcaster>(this);
    tf_timer_ = this->create_wall_timer(
            10ms, std::bind(&OrderPathPlanner::tf_timer_callback, this));
}

void OrderPathPlanner::create_marker_publisher() {
    marker_publisher_ = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 10);
    timer_ = this->create_wall_timer(
            1s, std::bind(&OrderPathPlanner::markers_timer_callback, this));
}


void OrderPathPlanner::create_position_listener() {
    pose_subscription_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
            "currentPosition", 10, std::bind(&OrderPathPlanner::position_callback, this, _1));
}

void OrderPathPlanner::position_callback(const geometry_msgs::msg::PoseStamped::SharedPtr msg) {
    std::cout << "Updating Robot Position: " << msg->pose.position.x << " , " << msg->pose.position.y << std::endl;
    data_handler->set_current_position(Vector2d(msg->pose.position.x, msg->pose.position.y));
}

void OrderPathPlanner::create_next_order_listener() {
    order_subscription_ = this->create_subscription<order_interfaces::msg::Order>(
            "nextOrder", 10, std::bind(&OrderPathPlanner::next_order_callback, this, _1));
}

void OrderPathPlanner::next_order_callback(const order_interfaces::msg::Order::SharedPtr msg) {
    std::cout << "Working on order " <<  std::to_string(msg->order_id);
    std::cout << " (" << msg->description << ")"<< std::endl;
    this->business_logic->process_order(std::to_string(msg->order_id), msg->description);
}

void OrderPathPlanner::tf_timer_callback() {
    rclcpp::Time now = this->get_clock()->now();
    geometry_msgs::msg::TransformStamped t;

    t.header.stamp = now;
    t.header.frame_id = "map";
    t.child_frame_id = "world";
    t.transform.translation.x = 0.0;
    t.transform.translation.y = 0.0;
    t.transform.translation.z = 0.0;
    t.transform.rotation.x = 0.0;
    t.transform.rotation.y = 0.0;
    t.transform.rotation.z = 0.0;
    t.transform.rotation.w = 1.0;

    tf_publisher_->sendTransform(t);
}

void OrderPathPlanner::markers_timer_callback() {
    auto markers = visualization_msgs::msg::MarkerArray(); 

    auto robot = create_robot();
    int id = 1;
    for(auto& position : data_handler->get_path()) {
        auto pickup = create_pickup(position->location, id++);
        markers.markers.push_back(pickup);
    }

    markers.markers.push_back(robot);
    marker_publisher_->publish(markers);
}

visualization_msgs::msg::Marker OrderPathPlanner::create_pickup(Vector2d location, int id) {
    visualization_msgs::msg::Marker pickup;
    pickup.header.frame_id = "world";
    pickup.header.stamp = this->get_clock()->now();
    pickup.ns = "pickup";
    pickup.id = id;
    pickup.type = visualization_msgs::msg::Marker::CYLINDER;
    pickup.action = visualization_msgs::msg::Marker::ADD;

    pickup.pose.position.x = location.x; 
    pickup.pose.position.y = location.y;
    pickup.pose.position.z = 50;
    pickup.scale.x = 100;
    pickup.scale.y = 100;
    pickup.scale.z = 100;
    pickup.color.a = 1.0;
    pickup.color.r = 1.0;
    pickup.color.g = 1.0;
    pickup.color.b = 1.0;

    return pickup;
}

visualization_msgs::msg::Marker OrderPathPlanner::create_robot() {
    visualization_msgs::msg::Marker robot;
    robot.header.frame_id = "world";
    robot.header.stamp = this->get_clock()->now();
    robot.ns = "robot";
    robot.id = 0;
    robot.type = visualization_msgs::msg::Marker::CUBE;
    robot.action = visualization_msgs::msg::Marker::ADD;

    robot.pose.position.x = this->data_handler->get_current_position().x;
    robot.pose.position.y = this->data_handler->get_current_position().y;
    robot.pose.position.z = 50;
    robot.scale.x = 100;
    robot.scale.y = 100;
    robot.scale.z = 300;
    robot.color.a = 0.8;
    robot.color.r = 1.0;
    robot.color.g = 1.0;
    robot.color.b = 1.0;

    return robot;
}

void BusinessLogic::process_order(std::string order_id, std::string description) {

    auto success_order = data_handler->get_order_from_id(order_id);

    if(!success_order.first) {
        std::cerr << "Order not found" << std::endl;
        return;
    }

    auto current_order = success_order.second;
    current_order->description = description;

    data_handler->set_end_position(Vector2d(current_order->location->x, 
                                            current_order->location->y));
    auto parts = data_handler->get_parts_from_order(current_order);
    auto path = this->optimal_path(parts);
    this->print_path(path);
    data_handler->save_path(path);
}

std::vector<std::shared_ptr<Part>> BusinessLogic::optimal_path(std::vector<std::shared_ptr<Part>> parts) {
    parts = this->merge_parts_on_same_position(parts);
    auto paths = this->generate_all_paths(parts);
    return pick_optimal_path(paths);
}

void BusinessLogic::print_path(std::vector<std::shared_ptr<Part>> path) {
    int count = 1;
    for(auto& part: path) {
        for(auto& product: part->required_for_products) {
            std::cout << count++ << ".\tFetching part \'" << part->part << "\' for product \'" << product;
            std::cout << "\' at x: " << part->location.x << " , y: " << part->location.y << std::endl;
        }
    }
    std::cout << "Delivering to destination x: " << data_handler->get_end_position().x;
    std::cout << " , y: " << data_handler->get_end_position().y << std::endl;
}

std::vector<std::shared_ptr<Part>> BusinessLogic::pick_optimal_path(std::vector<std::vector<std::shared_ptr<Part>>> paths) {
    std::vector<std::shared_ptr<Part>> optimal_path;
    _Float32 optimal_path_length = -1;
    for(auto& path: paths) {
        if(path.size() == 0) {
            return path;
        }
        auto path_length = this->length_of_path(path);
        if(path_length < optimal_path_length || optimal_path_length == -1) {
            optimal_path = path;
            optimal_path_length = path_length;
        }
    }
    if(optimal_path.size() == 0) {
        std::cerr << "No path found" << std::endl;
    }
    return optimal_path;
}

_Float32 BusinessLogic::length_of_path(std::vector<std::shared_ptr<Part>> path) {

    if(path.size() == 0) {
        return data_handler->get_current_position().distance_to(data_handler->get_end_position());
    } 

    _Float32 inter_parts_length = 0;
    for(size_t i=0; i<path.size()-1; i++) {
        inter_parts_length += path[i]->location.distance_to(path[i+1]->location);
    }

    return this->start_and_end_length(path) + inter_parts_length;
}

_Float32 BusinessLogic::start_and_end_length(std::vector<std::shared_ptr<Part>> path) {
    _Float32 first = path[0]->location.distance_to(data_handler->get_current_position());
    _Float32 last = path[path.size()-1]->location.distance_to(data_handler->get_end_position());
    return first + last;
}

std::vector<std::shared_ptr<Part>> BusinessLogic::merge_parts_on_same_position(std::vector<std::shared_ptr<Part>> parts) {
    std::vector<std::shared_ptr<Part>> merged_parts;
    for(auto& part: parts) {
        part->reset_required_for();
        if(merged_parts.empty()) {
            merged_parts.push_back(part);
        } else {
            bool merged = false;
            for(auto& merged_part: merged_parts) {
                if(part->part == merged_part->part && abs(part->location.equals(merged_part->location))) {
                    merged_part->required_for_products.push_back(part->required_for_products[0]);
                    merged = true;
                    break;
                } 
            }
            if(!merged) {
                merged_parts.push_back(part);
            }
        }
    }
    return merged_parts;
}

std::vector<std::vector<std::shared_ptr<Part>>> BusinessLogic::generate_all_paths(
        std::vector<std::shared_ptr<Part>> parts) {
    std::vector<std::vector<std::shared_ptr<Part>>> paths;

    std::sort(parts.begin(), parts.end());
    do
    {
        std::vector<std::shared_ptr<Part>> path;
        for ( auto x : parts ) {
            path.push_back(x);
        }
        paths.push_back(path);
    } while ( std::next_permutation( parts.begin(), parts.end() ) );

    return paths;
}


Vector2d Vector2d::distance_vector(Vector2d endpoint) {
    return Vector2d(endpoint.x - this->x,
            endpoint.y - this->y);
}

bool Vector2d::equals(Vector2d other) {
    return abs(this->x - other.x) < 0.01 && abs(this->y - other.y) < 0.01;
}

_Float32 Vector2d::distance_to(Vector2d endpoint) {
    Vector2d distance = this->distance_vector(endpoint);
    return distance.euclidean();
}

_Float32 Vector2d::euclidean() {
    return sqrt(this->x*this->x + this->y*this->y);
}


AsyncOrderFinder::AsyncOrderFinder(std::string path) {
    this->path = std::move(path);
}

OrderFinderTask::OrderFinderTask(std::string file_path, std::string order_id):
    file_path(file_path), order_id(order_id) {
    this->thread_done = std::make_shared<std::atomic_int>(0); 
}

void OrderFinderTask::start() {
    std::cerr << "[  TEST    ]  starting async task " << std::endl;
    task = std::async(std::launch::async, 
                    this->find_order_in_yaml, 
                    this->file_path, 
                    this->order_id,
                    this->thread_done);
}

std::shared_ptr<Order> OrderFinderTask::find_order_in_yaml(const std::string &path, const std::string &order_id, std::shared_ptr<std::atomic_int> thread_done) {
    YAML::Node yaml = YAML::LoadFile(path);
    assert(yaml.IsSequence());
    for(auto it : yaml) {
        if(thread_done->load() == TaskState::FOUND) {
            return std::make_shared<Order>(Order());
        }
        assert(it.IsMap());
        if(it["order"].as<std::string>() == order_id) {
            thread_done->store(TaskState::FOUND);
            return std::make_shared<Order>(Order(it));
        }
    }
    thread_done->store(TaskState::NOT_FOUND);
    return std::make_shared<Order>(Order());
}

void OrderFinderTask::stop() {
    this->thread_done->store(TaskState::FOUND);
}

std::vector<std::shared_ptr<OrderFinderTask>> AsyncOrderFinder::create_async_order_finders(const std::string order_id) {
    auto files = std::experimental::filesystem::directory_iterator(this->path);
    std::vector<std::shared_ptr<OrderFinderTask>> async_order_finders;

    for (auto& file : files) {
        auto shared_finder = std::make_shared<OrderFinderTask>(OrderFinderTask(file.path().string(),order_id));
        async_order_finders.push_back(shared_finder);
    }
    return async_order_finders;
}


std::pair<bool, std::shared_ptr<Order>> AsyncOrderFinder::wait_for_order_finders(std::vector<std::shared_ptr<OrderFinderTask>> async_orders) {
    while(get_nr_asyncs_completed(async_orders) < (int) async_orders.size()) {
        for(auto& async_order: async_orders) {
            if(TaskState::FOUND == async_order->found()) {
                return std::make_pair(true, async_order->get_order());
            }   
        }
    }

    return std::make_pair(false, std::make_shared<Order>(Order()));
}

int AsyncOrderFinder::get_nr_asyncs_completed(std::vector<std::shared_ptr<OrderFinderTask>> async_orders) {
    int nr_asynsc_completed = 0;
    for(auto& async_order: async_orders) {
        if (-1 == async_order->found()) {
            nr_asynsc_completed++;
        }
    }
    return nr_asynsc_completed;
}


void AsyncOrderFinder::destroy_async_order_finders(std::vector<std::shared_ptr<OrderFinderTask>> async_orders) {
    for(auto& async_order: async_orders) {
        async_order->stop();
    }
}

void AsyncOrderFinder::start_async_order_finders(std::vector<std::shared_ptr<OrderFinderTask>> async_orders) {
    for(auto& async_order: async_orders) {
        async_order->start();
    }
}

std::pair<bool, std::shared_ptr<Order>> AsyncOrderFinder::find_order_in_folder(const std::string &order_id) {

    auto async_orders = this->create_async_order_finders(order_id);
    this->start_async_order_finders(async_orders);
    auto success_order = this->wait_for_order_finders(async_orders);
    this->destroy_async_order_finders(async_orders);

    return success_order; 
}


Order::Order() {
    this->order = "-";
    this->location = std::make_unique<Vector2d>(0, 0);
}

Order::Order(std::string order, Vector2d location): 
    order(order), 
    location(std::make_unique<Vector2d>(location)) {}

Order::Order(YAML::Node yaml) {
    this->order = yaml["order"].as<std::string>();
    _Float32 cx = yaml["cx"].as<_Float32>();
    _Float32 cy = yaml["cy"].as<_Float32>();
    this->location = std::make_unique<Vector2d>(Vector2d(cx, cy));

    for(auto it : yaml["products"]) {
        this->product_names.push_back(it.as<std::string>());
    }
}

Part::Part() {}

Part::Part(YAML::Node yaml, std::string product) {
    this->part = yaml["part"].as<std::string>();
    this->required_for_products = {product};
    _Float32 cx = yaml["cx"].as<_Float32>();
    _Float32 cy = yaml["cy"].as<_Float32>();
    this->location = Vector2d(cx, cy);
}

void Part::reset_required_for() {
    this->required_for_products = {required_for_products[0]};
}

Product::Product() {
    this->id= "-";
}

Product::Product(YAML::Node yaml) {
    assert(yaml.IsMap());
    this->id = yaml["id"].as<std::string>();
    if(yaml["parts"]) {
        assert(yaml["parts"].IsSequence());
        for(auto it : yaml["parts"]) {
            this->parts.push_back(std::make_shared<Part>(Part(it, yaml["product"].as<std::string>())));
        }
    }
}

ProductCollector::ProductCollector(std::string path) {
    this->path = std::move(path);
}

void ProductCollector::collect_products_from_folder() {
    auto files = std::experimental::filesystem::directory_iterator(this->path);
    for (auto& file : files) {
        this->collect_products_from_yaml(file.path().string());
    }
}

void ProductCollector::collect_products_from_yaml(std::string file_path) {
    YAML::Node yaml = YAML::LoadFile(file_path);
    assert(yaml.IsSequence());
    for(auto it : yaml) {
        assert(it.IsMap());
        this->products.push_back(Product(it));
    }
}


FilePath::FilePath(std::string path, std::string addendum):  path(path) {
    this->add_to_path(addendum);
}

std::string FilePath::get_path() {
    return this->path;
}

bool FilePath::valid_directory() {
    return std::experimental::filesystem::is_directory(this->path);
}


bool FilePath::valid_source_path() {
    auto is_path = this->valid_directory(); 

    auto config = FilePath(this->path);
    config.add_to_path("/configuration/");
    bool has_config = config.valid_directory();

    auto orders = FilePath(this->path);
    orders.add_to_path("/orders/");
    bool has_orders = config.valid_directory();

    return is_path && has_config && has_orders;
}

void FilePath::add_to_path(std::string addendum) {
    if(this->path.back() != '/') {
        this->path += '/';
    }
    if(addendum.front() == '/') { 
        this->path += addendum.substr(1);
    }
    else {
        this->path += addendum;
    }
}

std::string FilePath::absolute_path() {
    if(this->path.front() == '/') {
        return path;
    } else {
        return ament_index_cpp::get_package_prefix("order_path_planner") + "/../../src/order_path_planner/" + path;
    }
}
