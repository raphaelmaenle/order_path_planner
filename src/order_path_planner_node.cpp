#include "order_path_planner/order_path_planner.hpp"

void print_usage();
int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<OrderPathPlanner>(argc, argv));
    rclcpp::shutdown();

    return 0;
}
