#include "gtest/gtest.h"
#include "order_path_planner/order_path_planner.hpp"


std::string absolute_path_of(std::string relative_path) {
    FilePath file(relative_path);
    return file.absolute_path();
}

class BusinessLogicTest: public ::testing::Test {
protected:
    std::string path = absolute_path_of("test/data/");
    std::shared_ptr<DataHandler> data_handler;
    std::shared_ptr<BusinessLogic> business_logic;
    void SetUp() {
        data_handler = std::make_shared<DataHandler>(path);
        business_logic = std::make_shared<BusinessLogic>(data_handler);
    }
};

TEST(LibraryTest, GTestTest) { 
    ASSERT_TRUE (true);
}

TEST(LibraryTest, RosSpinTest){
    rclcpp::init(0, nullptr);
    rclcpp::spin_some(std::make_shared<OrderPathPlanner>());
    rclcpp::shutdown();
    ASSERT_TRUE (true);
}

TEST(UnitTest, VectorDistanceTests){
    auto startpoint = Vector2d(0, 0);
    auto endpoint = Vector2d(1, 0);
    auto distance = startpoint.distance_to(endpoint);
    EXPECT_FLOAT_EQ(distance, 1.0);

    startpoint = Vector2d(0, 0);
    endpoint = Vector2d(0, 0);
    distance = startpoint.distance_to(endpoint);
    EXPECT_FLOAT_EQ(distance, 0.0);

    startpoint = Vector2d(0, 0);
    endpoint = Vector2d(1, 1);
    distance = startpoint.distance_to(endpoint);
    EXPECT_FLOAT_EQ(distance, sqrt(2));

    startpoint = Vector2d(1, -1);
    endpoint = Vector2d(-1, -2);
    distance = startpoint.distance_to(endpoint);
    EXPECT_FLOAT_EQ(distance, sqrt(5));
}


TEST(LibraryTest, YamlSimpleTest){
    auto path = absolute_path_of("test/data/test_01.yaml");
    YAML::Node doc = YAML::LoadFile(path);
    ASSERT_EQ(doc["test"].as<std::string>(), "test");
}

TEST(LibraryTest, YamlAdancedTest){
    auto path = absolute_path_of("test/data/test_02.yaml");
    YAML::Node doc = YAML::LoadFile(path);
    ASSERT_TRUE(doc.IsSequence());

    auto first_element = doc[0];
    ASSERT_TRUE(first_element.IsMap());
    ASSERT_EQ(first_element["order"].as<std::string>(), "1000001");
    EXPECT_FLOAT_EQ(first_element["cx"].as<_Float32>(), 748.944);
    EXPECT_FLOAT_EQ(first_element["cy"].as<_Float32>(), 474.71707);
    ASSERT_TRUE(first_element["products"].IsSequence());
    ASSERT_EQ(first_element["products"][0].as<std::string>(), "902");
    ASSERT_EQ(first_element["products"][1].as<std::string>(), "293");
}

TEST (LibraryTest, FileSystemAccess) {
    auto path = absolute_path_of("test/data/test_orders/");
    auto files = std::experimental::filesystem::directory_iterator(path);
    for (auto& file : files) {
        ASSERT_EQ(file.path().filename().string(), "orders_20201201.yaml");
        return;
    }
}

TEST (UnitTest, OrderFinderAsync) {
    auto path = absolute_path_of("test/data/orders/");
    AsyncOrderFinder finder(path);
    std::cerr << "[  TEST    ]  Activated Order Finder " << std::endl;

    auto success_order = finder.find_order_in_folder("1000001");
    ASSERT_TRUE(success_order.first);
    EXPECT_FLOAT_EQ(success_order.second->location->x, 748.944);

    success_order = finder.find_order_in_folder("1100001");
    ASSERT_TRUE(success_order.first);
    EXPECT_FLOAT_EQ(success_order.second->location->x, 481.03207);
}

TEST(UnitTest, PartCollectorYaml) {
    auto path = absolute_path_of("test/data/configuration/");
    ProductCollector collector(path);
    collector.collect_products_from_folder();
    ASSERT_EQ(collector.get_products().size(), size_t(1000));
}

TEST(PipelineTest, FindPartsFromOrder) {
    auto order_path = absolute_path_of("test/data/orders/");
    auto config_path = absolute_path_of("test/data/configuration/");

    ProductCollector collector(config_path);
    collector.collect_products_from_folder();

    AsyncOrderFinder finder(order_path);
    auto success_order = finder.find_order_in_folder("1000001");

    ASSERT_TRUE(success_order.first);

    std::vector<std::shared_ptr<Part>> parts;
    for(auto& product: collector.get_products()) {
        for(auto& required: success_order.second->product_names) {
            if(product.id == required) {
                for(auto& part: product.parts) {
                    parts.push_back(part);
                }
            }
        }
    }
    for(auto& part: parts) {
        std:: cerr << "[  TEST    ] " << part->part << std::endl;
    }
    ASSERT_EQ(parts.size(), size_t(12));
}


TEST_F(BusinessLogicTest, BusinessLogicGenerateIterations) {
    auto success_order = data_handler->get_order_from_id("1100001");
    auto parts = data_handler->get_parts_from_order(success_order.second);
    parts = business_logic->merge_parts_on_same_position(parts);
    auto paths = business_logic->generate_all_paths(parts);

    ASSERT_EQ(parts.size(), size_t(3));
    ASSERT_EQ(paths.size(), size_t(6));

    success_order = data_handler->get_order_from_id("1000000");
    parts = data_handler->get_parts_from_order(success_order.second);
    parts = business_logic->merge_parts_on_same_position(parts);
    paths = business_logic->generate_all_paths(parts);

    ASSERT_EQ(parts.size(), size_t(2));
    ASSERT_EQ(paths.size(), size_t(2));
}


TEST_F(BusinessLogicTest, BusinessLogic) {
    business_logic->process_order("1000001", "My Test Order");
    ASSERT_TRUE(true);
}


TEST_F(BusinessLogicTest, BusinessLogicMergeParts) {
    auto success_order = data_handler->get_order_from_id("1000001");
    auto parts = data_handler->get_parts_from_order(success_order.second);
    parts = business_logic->merge_parts_on_same_position(parts);
    ASSERT_EQ(parts.size(), size_t(3));

    success_order = data_handler->get_order_from_id("1000000");
    parts = data_handler->get_parts_from_order(success_order.second);
    parts = business_logic->merge_parts_on_same_position(parts);
    ASSERT_EQ(parts.size(), size_t(2));
}


TEST(UnitTest, LengthOfPath) {
    auto data_path = absolute_path_of("test/data/");
    auto data_handler = std::make_shared<DataHandler>(data_path);
    auto business_logic = std::make_shared<BusinessLogic>(data_handler);
    auto path = std::vector<std::shared_ptr<Part>>();
    path.push_back(std::make_shared<Part>(Part("Part X", Vector2d(1, 0))));
    path.push_back(std::make_shared<Part>(Part("Part Y", Vector2d(1, 1))));

    data_handler->set_current_position(Vector2d(0, 0));
    data_handler->set_end_position(Vector2d(0, 1));

    _Float32 path_length = business_logic->length_of_path(path);
    ASSERT_FLOAT_EQ(path_length, 3);

    path.push_back(std::make_shared<Part>(Part("Part Z", Vector2d(1, 2))));
    data_handler->set_end_position(Vector2d(2, 2));

    path_length = business_logic->length_of_path(path);
    ASSERT_FLOAT_EQ(path_length, 4);

}

TEST(UnitTest, FindOptimalPathLine) {
    auto data_path = absolute_path_of("test/data/");
    auto data_handler = std::make_shared<DataHandler>(data_path);
    auto business_logic = std::make_shared<BusinessLogic>(data_handler);
    auto path = std::vector<std::shared_ptr<Part>>();
    // criss-cross path added 
    path.push_back(std::make_shared<Part>(Part("Part 5", Vector2d(5, 5))));
    path.push_back(std::make_shared<Part>(Part("Part 2", Vector2d(2, 2))));
    path.push_back(std::make_shared<Part>(Part("Part 4", Vector2d(4, 4))));
    path.push_back(std::make_shared<Part>(Part("Part 1", Vector2d(1, 1))));
    path.push_back(std::make_shared<Part>(Part("Part 3", Vector2d(3, 3))));
    path.push_back(std::make_shared<Part>(Part("Part 6", Vector2d(6, 6))));

    data_handler->set_current_position(Vector2d(3, 3));
    data_handler->set_end_position(Vector2d(0, 0));

    auto optimal_path = business_logic->optimal_path(path);

    for (auto& part: optimal_path) {
        std::cerr << "[  TEST    ] " << part->part << std::endl;
    }

    _Float32 path_length = business_logic->length_of_path(optimal_path);
    ASSERT_FLOAT_EQ(path_length, 9*sqrt(2));

}

TEST(UnitTest, FindOptimalPathM) {
    auto data_path = absolute_path_of("test/data/");
    auto data_handler = std::make_shared<DataHandler>(data_path);
    auto business_logic = std::make_shared<BusinessLogic>(data_handler);
    auto path = std::vector<std::shared_ptr<Part>>();
    path.push_back(std::make_shared<Part>(Part("Part x", Vector2d(0, 2))));
    path.push_back(std::make_shared<Part>(Part("Part x", Vector2d(1, 1))));
    path.push_back(std::make_shared<Part>(Part("Part x", Vector2d(2, 2))));

    data_handler->set_current_position(Vector2d(0, 0));
    data_handler->set_end_position(Vector2d(2, 0));

    auto optimal_path = business_logic->optimal_path(path);

    for (auto& part: optimal_path) {
        std::cerr << "[  TEST    ] " << part->part << std::endl;
    }

    _Float32 path_length = business_logic->length_of_path(optimal_path);
    ASSERT_FLOAT_EQ(path_length, 4+2*sqrt(2));

}

TEST(UnitTest, FindOptimalPathCross) {
    auto data_path = absolute_path_of("test/data/");
    auto data_handler = std::make_shared<DataHandler>(data_path);
    auto business_logic = std::make_shared<BusinessLogic>(data_handler);
    auto path = std::vector<std::shared_ptr<Part>>();
    // criss-cross path added 
    path.push_back(std::make_shared<Part>(Part("Part 2", Vector2d(1, 1))));
    path.push_back(std::make_shared<Part>(Part("Part 1", Vector2d(1, 0))));

    data_handler->set_current_position(Vector2d(0, 0));
    data_handler->set_end_position(Vector2d(0, 1));

    auto optimal_path = business_logic->optimal_path(path);

    for (auto& part: optimal_path) {
        std::cerr << "[  TEST    ] " << part->part << std::endl;
    }

    _Float32 path_length = business_logic->length_of_path(optimal_path);
    ASSERT_FLOAT_EQ(path_length, 3);

}

TEST (LibraryTest, ROSPubSub) {
    rclcpp::init(0, nullptr);
    rclcpp::Node::SharedPtr node = rclcpp::Node::make_shared("TEST_pub_node");
    /*
    rclcpp::Publisher::SharedPtr publisher = node->advertise-><std_msgs::msg::String>("/test_topic", 10);

    std_msgs::msg::String msg;
    msg.data = "Hello World";
    publisher->publish(msg);

    rclcpp::Subscriber::SharedPtr subscriber = node->create_subscription<std_msgs::msg::String>("/test_topic", 10,
        [](std_msgs::msg::String::SharedPtr msg) {
            std::cerr << "[  TEST    ] " << msg->data << std::endl;
        });
        */
}
