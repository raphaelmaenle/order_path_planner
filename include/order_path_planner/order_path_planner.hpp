#include <chrono>
#include <thread>
#include <atomic>
#include <functional>
#include <memory>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>
#include <future>
#include <experimental/filesystem>

#include <yaml-cpp/yaml.h>
#include <ament_index_cpp/get_package_prefix.hpp>

#include "rclcpp/rclcpp.hpp"
#include "tf2_ros/transform_broadcaster.h"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "visualization_msgs/msg/marker_array.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "order_interfaces/msg/order.hpp"


class Vector2d {
    public:
        _Float32 x, y;
        Vector2d(): x(0), y(0) {};
        Vector2d(_Float32 x, _Float32 y): x(x), y(y) {};
        Vector2d distance_vector(Vector2d endpoint);
        _Float32 distance_to(Vector2d endpoint);
        bool equals(Vector2d other);
    private:
        _Float32 euclidean();
};

class Order {
    public:
        Order();
        Order(YAML::Node map);
        Order(std::string order, Vector2d location);
        std::string order;
        std::string description = "-";
        std::unique_ptr<Vector2d> location;
        std::vector<std::string> product_names;
};

class Part {
    public:
        Part();
        Part(YAML::Node yaml, std::string product);
        Part(std::string part, Vector2d location): part(part), location(location) {}
        Part(std::string part, Vector2d location, std::string product): part(part), location(location)
    {required_for_products = {product};}
        std::string part = "-";
        Vector2d location;
        std::vector<std::string> required_for_products = {"-"};
        void reset_required_for();
};

class Product {
    public:
        Product();
        Product(YAML::Node yaml);
        Product(std::string id, std::string product): id(id), product(product) {}
        std::vector<std::shared_ptr<Part>> parts;
        std::string id;
        std::string product;
};


class FileParser {
    public:
        FileParser(std::string path);
        ~FileParser();
        std::string parsePath(std::string path);
    private:
        std::string path;
        std::ifstream file;
};


class ProductCollector {
    public:
        ProductCollector(std::string path);
        void collect_products_from_folder();
        void collect_products_from_yaml(std::string file_path);
        std::vector<Product> get_products(){return products;}

    private:
        std::vector<Product> products;
        std::string path;
};


class OrderFinderTask {
public:
    OrderFinderTask();
    OrderFinderTask(std::string file_path, std::string order_id);
    int found() {return thread_done->load();}
    std::shared_ptr<Order> get_order() {return task.get();}
    void stop();
    void start();
    static std::shared_ptr<Order> find_order_in_yaml(const std::string &path, const std::string &order_id, std::shared_ptr<std::atomic_int> thread_done);
private:
    const std::string file_path;
    const std::string order_id;
    std::shared_ptr<std::atomic_int> thread_done;
    std::future<std::shared_ptr<Order>> task;
};

enum TaskState {
    NOT_FOUND = -1,
    NOT_DONE = 0,
    FOUND = 1
};

class AsyncOrderFinder {
    public:
        AsyncOrderFinder(std::string path);
        std::pair<bool, std::shared_ptr<Order>> find_order_in_folder(const std::string &order_id);
    private:
        std::string path;
        std::vector<std::shared_ptr<OrderFinderTask>> create_async_order_finders(const std::string order_id);
        void start_async_order_finders(std::vector<std::shared_ptr<OrderFinderTask>> async_orders);
        static std::pair<bool, std::shared_ptr<Order>> wait_for_order_finders(std::vector<std::shared_ptr<OrderFinderTask>> async_orders);
        static void destroy_async_order_finders(std::vector<std::shared_ptr<OrderFinderTask>> async_orders);
        static int get_nr_asyncs_completed(std::vector<std::shared_ptr<OrderFinderTask>> async_orders);
};


class FilePath {
    public:
        FilePath(std::string path):  path(path) {}
        FilePath(std::string path, std::string addendum);
        std::string get_path();
        void add_to_path(std::string addendum);
        std::string absolute_path();
        bool valid_source_path();
        bool valid_directory();
    private:
        std::string path;
};

class Map {
    public:
        Vector2d current_position;
        Vector2d end_position;
        std::vector<std::shared_ptr<Part>> path = {};
};

class DataHandler {
    public:
        DataHandler(std::string path);
        std::pair<bool, std::shared_ptr<Order>> get_order_from_id(std::string order_id);
        std::unique_ptr<ProductCollector> product_collector;
        std::vector<std::shared_ptr<Part>> get_parts_from_order(std::shared_ptr<Order> order);
        std::shared_ptr<Map> map;

        void save_path(std::vector<std::shared_ptr<Part>> path) {map->path = path;}
        std::vector<std::shared_ptr<Part>> get_path() {return map->path;}

        Vector2d get_end_position() { return map->end_position;}
        void set_end_position(Vector2d position) {map->end_position = position;}

        Vector2d get_current_position() {return map->current_position;}
        void set_current_position(Vector2d position) {map->current_position = position;}
    private:
        std::string get_conf_path(std::string path);
        std::string get_orders_path(std::string path);
        std::string conf_path;
        std::string order_path;

};


class BusinessLogic {
    public:
        BusinessLogic(std::shared_ptr<DataHandler> &data_handler): data_handler(data_handler) {}
        void process_order(std::string order_id, std::string desription);
        std::vector<std::shared_ptr<Part>> optimal_path(std::vector<std::shared_ptr<Part>> parts);
        std::vector<std::shared_ptr<Part>> merge_parts_on_same_position(std::vector<std::shared_ptr<Part>> parts);
        std::vector<std::vector<std::shared_ptr<Part>>> generate_all_paths(std::vector<std::shared_ptr<Part>> parts);
        std::vector<std::shared_ptr<Part>> pick_optimal_path(std::vector<std::vector<std::shared_ptr<Part>>> paths);
        _Float32 length_of_path(std::vector<std::shared_ptr<Part>> path);
        _Float32 start_and_end_length(std::vector<std::shared_ptr<Part>> path);
        void print_path(std::vector<std::shared_ptr<Part>> path);
    private:
        std::shared_ptr<DataHandler> data_handler;
};

class OrderPathPlanner: public rclcpp::Node {
    public:
        OrderPathPlanner(int argc, char** argv);
        OrderPathPlanner();
        void logger(std::string message);
    private:
        std::shared_ptr<DataHandler> data_handler;
        std::shared_ptr<BusinessLogic> business_logic;

        std::unique_ptr<Order> new_order;
        std::string arg_parser(int argc, char** argv);
        std::string get_default_path();
        void print_usage();

        visualization_msgs::msg::Marker create_pickup(Vector2d location, int id);
        visualization_msgs::msg::Marker create_robot();

        rclcpp::Subscription<order_interfaces::msg::Order>::SharedPtr order_subscription_;
        rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr pose_subscription_;
        rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr marker_publisher_;
        std::shared_ptr<tf2_ros::TransformBroadcaster> tf_publisher_;
        rclcpp::TimerBase::SharedPtr timer_;
        rclcpp::TimerBase::SharedPtr tf_timer_;

        void create_position_listener();
        void create_next_order_listener();
        void create_listeners();
        void create_publishers();
        void create_marker_publisher();
        void create_tf_publisher();
        void markers_timer_callback();
        void tf_timer_callback();
        void position_callback(const geometry_msgs::msg::PoseStamped::SharedPtr msg);
        void next_order_callback(const order_interfaces::msg::Order::SharedPtr msg);

};
